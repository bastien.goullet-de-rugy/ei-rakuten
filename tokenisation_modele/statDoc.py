import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math
from textblob import TextBlob



def etudeStatYtrain():
    dataYtrain=pd.read_csv('Ytrain.csv')
    l,r=dataYtrain.shape
    dt={}
    for id in range(l):
        classe=int(dataYtrain['prdtypecode'][id])
        if classe in dt.keys():
            dt[classe].add(id)
        else:
            dt[classe]={id}
    # nombre de classe pour le doc

    nbClass=len(dt.keys()) # 27 pour le cas étudié

    # tracé du graphe de nb de produit en fonction des classes 
    def traceClasse():
        X=[]
        Y=[]
        for classe in sorted(dt.keys()):
            X.append(classe)
            Y.append(len(dt[classe]))
        plt.style.use('dark_background')
        plt.figure(figsize=(15, 12))
        sns.barplot(x=X,y=Y)
        plt.show()
    print('nombre de classe : ',nbClass,'\nnombre de produit : ',l)
    traceClasse()


dfX = pd.read_csv('data/Xtrain.csv')
dfY = pd.read_csv('data/Ytrain.csv')


def pagesVides():
    pagesVides = {}
    pagesVides['vide'] = 0
    pagesVides['non_vide'] = 0
    for i in range(len(dfX)):
        description = dfX.iloc[i]['description']
        if type(description) == float and math.isnan(description) :
            pagesVides['vide'] +=1
        else: 
            pagesVides['non_vide'] += 1
    n = somme(pagesVides)
    pagesVides['vide'] /= n
    pagesVides['non_vide'] /= n 
    return pagesVides



def somme(dict): 
    somme = 0
    for key in dict.keys():
        somme += dict[key]
    return somme 

