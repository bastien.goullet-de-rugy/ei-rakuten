import pickle
from math import *
from collections import Counter,OrderedDict

 
vocabFile=open('saveFiles/vocabulary.txt','rb')
vocab=pickle.load(vocabFile)
vocabFile.close()

indexInvFile=open('saveFiles/indexInversed.txt','rb')
indexInversed=pickle.load(indexInvFile)
indexInvFile.close()


def tf(term,productId,indInv):
    return indInv[term][productId]

# pondération logarithmique d'un terme
def tfLog(term,productId, indInv):
    tf = tf(term,productId, indInv)
    if tf > 0:
        return 1 +log(tf)
    else:
        return 0
    

def statText(text):
    counter= Counter()
    for term in text:
        counter.update([term])
    stats={}
    stats["freqMax"] = counter.most_common(1)[0][1]
    stats["uniqueTerms"] = len(counter.items())
    tf_moy = sum(counter.values())
    stats["freqMoy"] = tf_moy/len(counter.items())
    return stats


def statsCollection(collection):
    stats={}
    l,r=collection.shape
    stats["nb_docs"]=l
    for index in range(l):
        stats[collection['productid'][index]] = statText(collection['text'][index])
    return stats

def tfNormalise(term,productId,indInv,stats_collection):
        tf = tf(term,productId,indInv)
        tfNormalise = 0.5 + 0.5 * (tf /stats_collection[productId]["freq_max"])
        return tfNormalise

def tfLogNormalise(term,productId, indInv,stats_collection):
        tf = tf(term,productId, indInv)
        tfLogNormalise = (1 +log(tf))/(1 + log(stats_collection[productId]["freq_moy"]))
        return tfLogNormalise
    

def idf(term,indInv,nbDoc):
    return log(nbDoc/len(indInv[term].keys()))


def processing_vectorial_query(query, inverted_index, stats_collection, weighting_scheme_document,weighting_scheme_query):
    relevant_docs = {}
    counter_query= Counter()
    nb_doc = stats_collection["nb_docs"]
    norm_query=0.
    for term in query:
        w_term_query=0.
        counter_query.update([term])
        if weighting_scheme_query=="binary":
            w_term_query = 1
        if weighting_scheme_query=="frequency":
            w_term_query = counter_query[term]
        norm_query = norm_query + w_term_query*w_term_query
        for doc in inverted_index[term]:
            w_term_doc = 0.
            relevant_docs[doc]=0.
            if weighting_scheme_document=="binary":
                w_term_doc=1
            if weighting_scheme_document=="frequency":
                w_term_doc = tf(term,doc,inverted_index)
            if weighting_scheme_document=="tf_idf_normalize":
                w_term_doc = tfNormalise(term,doc, inverted_index,stats_collection)*idf(term,inverted_index,nb_doc)
            if weighting_scheme_document=="tf_idf_logarithmic":
                w_term_doc = tfLog (term,doc, inverted_index)*idf(term,inverted_index,nb_doc)
            if weighting_scheme_document=="tf_idf_logarithmic_normalize":
                w_term_doc = tfLogNormalise (term,doc, inverted_index,stats_collection)*idf(term,inverted_index,nb_doc)
            relevant_docs[doc] = relevant_docs[doc] + w_term_doc*w_term_query
    ordered_relevant_docs = OrderedDict(sorted(relevant_docs.items(), key=lambda t: t[1], reverse=True))
    return ordered_relevant_docs


