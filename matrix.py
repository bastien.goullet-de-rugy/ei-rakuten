from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import math as m

from scipy import sparse



tfidf = TfidfVectorizer()

# train=pd.read_csv('data/XYtestProcessed.csv', names=['IntegerID', 'Title', 'Description', 'ProductID', 'ImageID', 'ProductTypeCode', 'TitleAndDescription'],skiprows=[0])

# Xtfidf=tfidf.fit_transform(train["TitleAndDescription"])

# sparse.save_npz("data/XtestAdapted.npz", Xtfidf)
train=pd.read_csv('data/XYtrainProcessed.csv', names=['IntegerID', 'Title', 'Description', 'ProductID', 'ImageID', 'ProductTypeCode', 'TitleAndDescription'],skiprows=[0])

Xtfidf=tfidf.fit_transform(train["TitleAndDescription"])

sparse.save_npz("data/XtrainAdapted.npz", Xtfidf)


