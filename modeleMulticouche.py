import pandas
import numpy as np
#fonction d'activation
def logistique(x):
    return 1.0/(1 + np.exp(-x))

def logistique_deriv(x):
    return logistique(x) * (1 - logistique(x))
#taux d'apprentissage(learning rate)
LR = 1   

taille_prem_couch = 3
taille_couche_cachee = 4

#nombres d'itérations
nb_passages = 40

#pour avoir des valeurs initiales de nos branches qui ne changent pas à chaque fois qu'on lance le programm
np.random.seed(1)
#poids des branches pour la couche d'entrée 
poids_entree = np.random.uniform(-1, 1, (taille_prem_couch, taille_couche_cachee))
#poids des branches pour la couche caché, on en a qu'une ici
poids_caches = np.random.uniform(-1, 1, taille_couche_cachee)

pre_activ_h = np.zeros(taille_couche_cachee)
post_activ_h = np.zeros(taille_couche_cachee)

donnee_train = pandas.read_excel('X_train_simplifie.xlsx')
res_souhai = donnee_train.output
donnee_train = donnee_train.drop(['output'], axis=1)
donnee_train = np.asarray(donnee_train)
taille_entree = len(donnee_train[:,0])

validation_ex = pandas.read_excel('MLP_Vdata.xlsx')
valid_result = validation_ex.output
validation_ex = validation_ex.drop(['output'], axis=1)
validation_ex = np.asarray(validation_ex)
taille_valid = len(validation_ex[:,0])

#entrainement
for passage in range(nb_passages):
    for echantillon in range(taille_entree):
        for noeud in range(taille_couche_cachee):
            pre_activ_h[noeud] = np.dot(donnee_train[echantillon,:], poids_entree[:, noeud])
            post_activ_h[noeud] = logistique(pre_activ_h[noeud])
            
        post_act_fin = np.dot(post_activ_h, poids_caches)
        post_act_fin = logistique(post_act_fin)
        
        FE = post_act_fin - res_souhai[echantillon]
        
        for noeud_cache in range(taille_couche_cachee):
            gradiant_erreur = FE * logistique_deriv(post_act_fin)
            erreur_cache = gradiant_erreur * post_activ_h[noeud_cache]
                       
            for I_noeud in range(taille_prem_couch):
                input_value = donnee_train[echantillon, I_noeud]
                gradient_ItoH = gradiant_erreur * poids_caches[noeud_cache] * logistique_deriv(pre_activ_h[noeud_cache]) * input_value
                
                poids_entree[I_noeud, noeud_cache] -= LR * gradient_ItoH
                
            poids_caches[noeud_cache] -= LR * erreur_cache

#cofirmation      
nb_de_bonnes_rep = 0
for echantillon in range(taille_valid):
    for noeud in range(taille_couche_cachee):
        pre_activ_h[noeud] = np.dot(validation_ex[echantillon,:], poids_entree[:, noeud])
        post_activ_h[noeud] = logistique(pre_activ_h[noeud])
            
    post_act_fin = np.dot(post_activ_h, poids_caches)
    post_act_fin = logistique(post_act_fin)
        
    if post_act_fin > 0.5:
        output = 1
    else:
        output = 0     
        
    if output == valid_result[echantillon]:
        nb_de_bonnes_rep += 1

print('Pourcentage de classification correctes')
print(nb_de_bonnes_rep*100/taille_valid)

#Maxence doit compléter le calcul du rappel pour le f1