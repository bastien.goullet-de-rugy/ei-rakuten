import pandas as pd
import numpy as np
from sklearn import tree

X_train= pd.read_excel('X_train_vect_8k.xlsx')
Y_train = pd.read_excel('categorie_train_vect_8k.xlsx')
clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, Y)

def compare(res, valid) : 
    nb_bonne_rep = 0
    if not(len(res) == len(valid)):
        return "pas la même taille"
    else : 
        for i in range(len(res)):
            if res[i] == valid[i] : 
                nb_bonne_rep += 1
    return (nb_bonne_rep/len(res))*100



#valeurs de test prise au sein du Xtrain 
X_test = pd.read_excel('X_test_vect_200.xlsx')
Y_test = pd.read_excel('X_test_vect_200.xlsx')
resultats = clf.predict()
precision = compare(resultats, Y_test)

print("precision de" + precision + "%")


