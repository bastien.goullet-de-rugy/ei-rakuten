import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import model_selection, svm
from sklearn.metrics import accuracy_score
from lightgbm import train
from scipy import sparse


train_csv = pd.read_csv('data/XYtrainProcessed.csv', index_col=0)
X_tfidf_sample = sparse.load_npz("data/XtrainAdaptedDescr.npz")



SVM = svm.SVC(C=1.0, kernel='linear', degree=3, gamma='auto')
SVM.fit(X_tfidf_sample[0:80000],train_csv["ProductTypeCode"][0:80000])
# predict labels
predictions_SVM = SVM.predict(X_tfidf_sample[80001:84915])
# get the accuracy
print("Accuracy: ",accuracy_score(predictions_SVM, train_csv["ProductTypeCode"][80001:84915])*100)


